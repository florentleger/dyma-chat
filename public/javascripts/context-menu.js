window.addEventListener("DOMContentLoaded", () => {
  const menu = document.querySelector("#context-menu");
  const contextMenuActive = "context-menu--active";
  let menuState = 0;
  let roomElem;
  let listRooms;

  document.addEventListener("contextmenu", (e) => {
    roomElem = clickOnElem(e, "item-room");
    listRooms = clickOnElem(e, "list-rooms");

    if (roomElem) {
      e.preventDefault();
      openMenu(e);
    } else if (listRooms) {
      e.preventDefault();
      openMenu(e, "createOnly");
    } else {
      roomElem = null;
      closeMenu();
    }
  });

  document.addEventListener("click", (e) => {
    const menuOption = clickOnElem(e, "context-menu-link");
    if (menuOption) {
      e.preventDefault();
      menuItemListener(menuOption);
    } else {
      closeMenu();
    }
  });

  function clickOnElem(e, elem) {
    return e.srcElement.classList.contains(elem) ? e.srcElement : false;
  }

  function openMenu(e, mode) {
    createEditMenuElems(mode);
    menu.style.left = `${e.pageX}px`;
    menu.style.top = `${e.pageY}px`;
    if (menuState !== 1) {
      menuState = 1;
      menu.classList.add(contextMenuActive);
    }
  }

  function createEditMenuElems(mode) {
    menu.innerHTML = "";

    const createLi = document.createElement("li");
    createLi.classList.add("context-menu-item");
    createLi.innerHTML = `
    <a href="#" class="context-menu-link" data-action="create"
      ><i class="fa fa-plus mr-1"></i> Ajouter une room</a
    >`;

    const editLi = document.createElement("li");
    editLi.classList.add("context-menu-item");
    editLi.innerHTML = `
    <a href="#" class="context-menu-link" data-action="edit"
      ><i class="fa fa-edit mr-1"></i> Editer la room</a
    >`;

    const deleteLi = document.createElement("li");
    deleteLi.classList.add("context-menu-item");
    deleteLi.innerHTML = `
    <a href="#" class="context-menu-link" data-action="delete"
      ><i class="fa fa-times mr-1"></i> Supprimer la room</a
    >`;

    if (mode === "createOnly") {
      menu.append(createLi);
      menu.style.height = "60px";
    } else {
      menu.append(editLi, deleteLi, createLi);
      menu.style.height = "150px";
    }
  }

  function closeMenu() {
    if (menuState !== 0) {
      menuState = 0;
      menu.classList.remove(contextMenuActive);
    }
  }

  function menuItemListener(link) {
    const action = link.getAttribute("data-action");
    if (action === "create") {
      listRooms
        ? listRooms.appendChild(createEditableRoomItem({ action }))
        : roomElem.parentNode.appendChild(createEditableRoomItem({ action }));
    } else {
      const room = rooms[roomElem.getAttribute("data-index")];
      if (action === "edit") {
        roomElem.replaceWith(createEditableRoomItem({ room, action }));
      } else if (action === "delete") {
        ioClient.emit("deleteRoom", room);
      }
    }
    closeMenu();
  }
});
